---
Title: Refactor code into modern languages with AI-powered GitLab Duo 
---


Whether you are tasked with modernizing the code base or framework by switching to a new programming language, or it requires knowledge about new language features in the same language -- AI-powered GitLab Duo can help. Learn how to approach code refactoring challenges, and dive into best practices and examples from the past 20 years. 

The prompts and examples in this blog post are shown in different IDEs: VS Code and JetBrains IDEs, for example IntelliJ IDEA, PyCharm, CLion. The IDEs are using the latest versions (VS Code 1.91.1, IntelliJ IDEA 2024.1.4, PyCharm 2023.3.5, CLion 2024.1.4), with the [GitLab Duo extensions/plugins](https://docs.gitlab.com/ee/user/project/repository/code_suggestions/supported_extensions.html) installed (JetBrains: 2.5.0, VS Code: v4.21.0)

The development environment uses the latest release on GitLab.com SaaS, including the updates to Anthropic Claude 3.5 for Code Suggestions and Chat. Spoiler: They are even more powerful and efficient.


## Refactor code to modern programming language standards

The next sections show how to refactor, explain, and understand the language specific differences in a defined programming language. 

### Generate Java 7 and refactor to Java 8 

A customer asked whether GitLab Duo supports Java 7. While testing the Code Suggestions and Chat prompts to generate Java 7 specific code, I also thought of migration help. The underlying Large Language Models with Claude 3.5 are capable of understanding the language specific differences. 

The following example shows a prompt to generate Java 7 code with Duo Code Suggestions.

```java
// Generate a new class for a File Manager. Use Java 7 features only.
import java.io.*;
import java.nio.file.*;
import java.util.*;

public class FileManager {
    private Path rootDirectory;

    public FileManager(String rootPath) throws IOException {
        this.rootDirectory = Paths.get(rootPath);
        if (!Files.exists(rootDirectory)) {
            Files.createDirectories(rootDirectory);
        }
    }

    public void createFile(String fileName, String content) throws IOException {
        Path filePath = rootDirectory.resolve(fileName);
        Files.write(filePath, content.getBytes(), StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
    }

    public String readFile(String fileName) throws IOException {
        Path filePath = rootDirectory.resolve(fileName);
        return new String(Files.readAllBytes(filePath));
    }

    public void deleteFile(String fileName) throws IOException {
        Path filePath = rootDirectory.resolve(fileName);
        Files.delete(filePath);
    }

    public List<String> listFiles() throws IOException {
        List<String> fileList = new ArrayList<>();
        try (DirectoryStream<Path> stream = Files.newDirectoryStream(rootDirectory)) {
            for (Path file : stream) {
                fileList.add(file.getFileName().toString());
            }
        }
        return fileList;
    }

    public void copyFile(String sourceFileName, String destinationFileName) throws IOException {
        Path sourcePath = rootDirectory.resolve(sourceFileName);
        Path destinationPath = rootDirectory.resolve(destinationFileName);
        Files.copy(sourcePath, destinationPath, StandardCopyOption.REPLACE_EXISTING);
    }

    public void moveFile(String sourceFileName, String destinationFileName) throws IOException {
        Path sourcePath = rootDirectory.resolve(sourceFileName);
        Path destinationPath = rootDirectory.resolve(destinationFileName);
        Files.move(sourcePath, destinationPath, StandardCopyOption.REPLACE_EXISTING);
    }

    public long getFileSize(String fileName) throws IOException {
        Path filePath = rootDirectory.resolve(fileName);
        return Files.size(filePath);
    }

    public boolean fileExists(String fileName) {
        Path filePath = rootDirectory.resolve(fileName);
        return Files.exists(filePath);
    }
}
```

You can continue with the following steps:

1. Validate the generated code with the `/explain` slash command in Duo Chat.

![](assets/images/2024-07-duo-refactoring/intellij_java7_generate_refactor.png)

2. Refactor the Java 7 code into Java 8 using the `/refactor` slash command in Duo Chat.
3. How to generate Java 8 specific code using Code Suggestions. 
4. Continue writing Java 8 code in the same context with AI-powered code completions.

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/XKRv6uBkD2I" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->


### Refactor C++03 to C++11 to C++14 to C+17 to C++20 

The C++ standard has a long history and can be considered stable and mature. Depending on the target architecture and platforms, the supported standard depends on the compiler on the host operating system. In my career, I needed to support 25 different Linux and Windows distributions in open source projects, and have seen varying compiler support for newer language features and C++ standards. In 2015 we settled on C++11 features, and later never touched based on C++14 or C++17. Refactoring functions and algorithms to a new C++ standard would have required too much effort, and help explaining the differences was needed. Back then, we researched through books, online search, Stack Overflow, and sometimes tools assistance for refactoring.

Things have changed with AI-powered assistants and GitLab Duo: You can instruct Duo Code Suggestions and Chat to provide source code in a specific C++ standard. It is also possible to compare the different outputs then. 

```c++
/* Create a config parser which can parse an ini format
Use C++03 standard only
 */

#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <sstream>

class ConfigParser {
private:
    std::map<std::string, std::map<std::string, std::string> > config;

public:
    ConfigParser() {}

    bool parse(const std::string& filename) {
        std::ifstream file(filename.c_str());
        if (!file.is_open()) {
            return false;
        }

        std::string line;
        std::string currentSection;

        while (std::getline(file, line)) {
            // Trim whitespace
            size_t first = line.find_first_not_of(" \t");
            size_t last = line.find_last_not_of(" \t");
            if (first == std::string::npos) continue; // Skip empty lines
            line = line.substr(first, (last - first + 1));

            if (line[0] == '[' && line[line.length() - 1] == ']') {
                // This is a section
                currentSection = line.substr(1, line.length() - 2);
            } else {
                // This is a key-value pair
                size_t delimiterPos = line.find('=');
                if (delimiterPos != std::string::npos) {
                    std::string key = line.substr(0, delimiterPos);
                    std::string value = line.substr(delimiterPos + 1);
                    config[currentSection][key] = value;
                }
            }
        }

        file.close();
        return true;
    }

    std::string getValue(const std::string& section, const std::string& key) const {
        std::map<std::string, std::map<std::string, std::string> >::const_iterator sectionIt = config.find(section);
        if (sectionIt != config.end()) {
            std::map<std::string, std::string>::const_iterator keyIt = sectionIt->second.find(key);
            if (keyIt != sectionIt->second.end()) {
                return keyIt->second;
            }
        }
        return "";
    }

    void printConfig() const {
        std::map<std::string, std::map<std::string, std::string> >::const_iterator sectionIt;
        for (sectionIt = config.begin(); sectionIt != config.end(); ++sectionIt) {
            std::cout << "[" << sectionIt->first << "]" << std::endl;
            std::map<std::string, std::string>::const_iterator keyIt;
            for (keyIt = sectionIt->second.begin(); keyIt != sectionIt->second.end(); ++keyIt) {
                std::cout << keyIt->first << " = " << keyIt->second << std::endl;
            }
            std::cout << std::endl;
        }
    }
};

int main() {
    ConfigParser parser;
    if (parser.parse("config.ini")) {
        std::cout << "Config file parsed successfully." << std::endl;
        parser.printConfig();

        std::cout << "Value of 'key1' in 'section1': " << parser.getValue("section1", "key1") << std::endl;
    } else {
        std::cout << "Failed to parse config file." << std::endl;
    }
    return 0;
}
```

The same prompts can be applied to [explaining code](https://docs.gitlab.com/ee/user/gitlab_duo_chat/examples.html#explain-code-in-the-ide), [generating tests](https://docs.gitlab.com/ee/user/gitlab_duo_chat/examples.html#write-tests-in-the-ide) and even [refactoring code](https://docs.gitlab.com/ee/user/gitlab_duo_chat/examples.html#refactor-code-in-the-ide). 

The following C++ standards have been tested working with GitLab Duo Code Suggestions:

- C++03
- C++11
- C++14
- C++17
- C++20
- C++23

You can find the source code in the [Challenge - Refactor Cpp Standards Config Parser](https://gitlab.com/gitlab-da/use-cases/ai/ai-workflows/gitlab-duo-challenges/code-challenges/challenge-refactor-cpp-standards-config-parser) project. 

There are many different combinations to refactor source code between the C++ standards. The following examples reflect on modernizing the C++ code with version upgrades, but also test a downgrade where ambitious developers wanted to use C++20 but had to use C++11 to support enterprise customer environments. 

#### Migration: Refactor C++03 into C++14 

The goal is to modernize the code, and understand the changes between C++03 and C++14. Open the [03.cpp](https://gitlab.com/gitlab-da/use-cases/ai/ai-workflows/gitlab-duo-challenges/code-challenges/challenge-refactor-cpp-standards-config-parser/-/blob/main/03.cpp?ref_type=heads) source code in IDE, select it, and use the following Duo Chat prompt: 

```markdown
/refactor into C++14 and explain the changes

...

Explanations of the changes:

1. Default constructor: Changed `ConfigParser() {}` to `ConfigParser() = default;` to use the default constructor.
2. File opening: Removed `.c_str()` when opening the file, as C++11 and later support `std::string` directly.
3. Lambda functions: Introduced a lambda function trim to handle string trimming, making the code more readable and reusable.
4. Auto keyword: Used `auto` for type deduction in various places, improving code readability.
5. Range-based for loops: In the `printConfig` method, replaced traditional for loops with range-based for loops, which are more concise and less error-prone.
6. Structured bindings: Used structured bindings in the range-based for loops to directly access key-value pairs.
7. If statements with initializers: In the `getValue` method, used if statements with initializers to combine the find operation and the check in a single line.
8. String member functions: Used `front()` and `back()` instead of array-style access for the first and last characters of strings.
9. Uniform initialization: Used uniform initialization `{}` instead of parentheses `()` for object creation.
10. Removed `std::endl`: Replaced `std::endl` with `'\n'` for better performance, as `std::endl` forces a flush of the output buffer.

These changes make the code more modern, concise, and potentially more efficient while maintaining the same functionality.

```

![](assets/images/2024-07-duo-refactoring/duo_refactor_cpp_clion_chat_cpp03_to_14_01.png)

![](assets/images/2024-07-duo-refactoring/duo_refactor_cpp_clion_chat_cpp03_to_14_02.png)


#### Downgrade: Refactor C++23 to C++11

Sometimes, developers started a new feature proof-of-concept in a modern C++ standard like C++20. The actual supported versions for enterprise customer environments require older compilers with C++11 support. Downgrading the versions come at a cost, and require evaluating which language features are still supported, and which other disadvantages could come across.

Open the [23.cpp](https://gitlab.com/gitlab-da/use-cases/ai/ai-workflows/gitlab-duo-challenges/code-challenges/challenge-refactor-cpp-standards-config-parser/-/blob/main/23.cpp?ref_type=heads) source code in the IDE, select it, and use the following Duo Chat prompt: 

```markdown
/refactor into C++11, explain the necessary changes and potential problems

The main changes required to make this code compatible with C++11 are:

1. Remove the C++23 standard comment, as it's no longer applicable.
2. Replace `std::string::find_first_not_of` and `std::string::find_last_not_of` with C++11 compatible alternatives in the trim function.
3. Use `const auto&` instead of `auto` for loop variables to avoid unnecessary copying.

...

Potential problems in the original code:

1. The original code used C++23 features, which might not be widely supported yet.
2. The `trim` function used `std::string::find_first_not_of` and `std::string::find_last_not_of`, which are not available in C++11.
3. The use of `auto` instead of const auto& in some loops could lead to unnecessary copying of complex objects.

These issues have been addressed in the refactored version. The `trim` function now uses `std::find_if_not` with lambda functions to achieve the same result in a C++11 compatible way. The use of `const auto&` ensures that no unnecessary copying occurs when iterating over the config map.
```

![](assets/images/2024-07-duo-refactoring/duo_refactor_cpp_clion_chat_cpp23_to_11_01.png)

![](assets/images/2024-07-duo-refactoring/duo_refactor_cpp_clion_chat_cpp23_to_11_02.png)


### Explain and refactor COBOL

You can use GitLab Duo to explain the source code, analyze, fix and refactor for COBOL programs. I have never written nor learned COBOL, and found this helpful [COBOL Programming Course](https://github.com/openmainframeproject/cobol-programming-course) with many examples.

I then asked Duo Chat how to get started with COBOL, create a COBOL program, compile a COBOL program on macOS.

```markdown
Please explain what COBOL is and its syntax

Please create a COBOL program that shows the first steps

Tell me more about the COBOL compiler. Which system do I need? Can I do it on my macOS?
```

![](assets/images/2024-07-duo-refactoring/vscode_chat_cobol_generate_example.png)

Open a COBOL program, select the source code and ask Duo Chat to `/explain` its purpose and functionality.

You can also refine the prompts to get more high-level summaries, for example:

```
/explain like I am five
```

Tip: Programming languages share similar algorithms and functionality. For COBOL, Duo Chat offered to explain it using Python, and therefore, I adjusted future prompts to ask for an explanation in Python.

```markdown
/explain in a different programming language
```

You can also use the `/refactor` slash command in Duo Chat to improve the code quality, fix potential problems, and try to refactor COBOL into Python.

```markdown
/refactor fix the environment error

/refactor fix potential problems

/refactor into Python
```

The [GitLab Duo Coffee Chat - Challenge: Explain and Refactor COBOL programs](https://gitlab.com/gitlab-da/use-cases/ai/ai-workflows/gitlab-duo-challenges/code-challenges/challenge-explain-refactor-cobol-program) recording shows all discussed steps in a practical use case, including how to find a missing period: 

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/pwlDmLQMMPo" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

## Refactor a language into another language

Modernization and code quality improvements sometimes require the change of a programming language. Similar refactor prompts with GitLab Duo can help speed up the migration process. The COBOL example with Python is just one of many requirements in enterprise environments -- lets dive into more use cases.

### Refactor C to Rust 

In early 2024, several programming languages, like C, have been called out for not being memory safe. The recommendations for future projects include memory safe languages like Rust. But how to start a migration, and what are the challenges?

Lets try it with a simple example in C. The code was generated using Duo Code Suggestions and should print the basic operating system information, like the name, version, and platform. The C code compiles cross-platform on Windows, Linux and macOS.

```c
// Read OS files to identify the platform, name, versions
// Print them on the terminal
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef _WIN32
    #include <windows.h>
#elif __APPLE__
    #include <sys/utsname.h>
#else
    #include <sys/utsname.h>
#endif

void get_os_info() {
    #ifdef _WIN32
        OSVERSIONINFOEX info;
        ZeroMemory(&info, sizeof(OSVERSIONINFOEX));
        info.dwOSVersionInfoSize = sizeof(OSVERSIONINFOEX);
        GetVersionEx((OSVERSIONINFO*)&info);

        printf("Platform: Windows\n");
        printf("Version: %d.%d\n", info.dwMajorVersion, info.dwMinorVersion);
        printf("Build: %d\n", info.dwBuildNumber);
    #elif __APPLE__
        struct utsname sys_info;
        uname(&sys_info);

        printf("Platform: macOS\n");
        printf("Name: %s\n", sys_info.sysname);
        printf("Version: %s\n", sys_info.release);
    #else
        struct utsname sys_info;
        uname(&sys_info);

        printf("Platform: %s\n", sys_info.sysname);
        printf("Name: %s\n", sys_info.nodename);
        printf("Version: %s\n", sys_info.release);
    #endif
}

int main() {
    get_os_info();
    return 0;
}
```

Open the source code in [`os.c`](https://gitlab.com/gitlab-da/use-cases/ai/ai-workflows/gitlab-duo-challenges/code-challenges/challenge-refactor-c-to-rust/-/blob/897bf57a14bb7be07d842e7f044f93a61456d611/c/os.c) in JetBrains CLion, for example. Select the source code and ask Duo Chat to `/explain` its purpose and functionality. Next, ask Duo Chat to `/refactor` the C code, and take it one step further: `/refactor into Rust`. 

Initialize a new Rust project, and copy the generated source code into the `src/main.rs` file. Run `cargo build` to compile the code. 

![](assets/images/2024-07-duo-refactoring/jetbrains_clion_c_rust.png)

In the [GitLab Duo Coffee Chat: Challenge - Refactor C into Rust](https://gitlab.com/gitlab-da/use-cases/ai/ai-workflows/gitlab-duo-challenges/code-challenges/challenge-refactor-c-to-rust) recording, you'll see a compilation error which gets fixed with the help of Duo Chat and `/refactor` slash command. The session also shows how to improve the maintanability of the new Rust code by adding more error handling. 

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/nf8g2ucqvkI" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->


### Refactor Perl to Python 

That one script that runs on production servers, does its job, the author left the company ten years ago, and nobody wants to touch it. The problem might also apply to multiple scripts, or even a whole application. A decision was made to migrate everything to modern Python 3, with the goal to modernize the code, and understand the changes between Perl and Python.

A customer recently asked in a GitLab Duo workshop whether a direct migration is possible using GitLab Duo. Short answer - yes, it is. Longer answer: You can use refined Duo chat prompts to refactor Perl code into Python, similar to other examples in this blog post.

Open the [script.pl]() source code in IDE, select it, and open Duo Chat.

```perl
#!/usr/bin/perl
use strict;
use warnings;

open my $md_fh, '<', 'file.md' or die "Could not open file.md: $!";

my $l = 0;
my $e = 0;
my $h = 0;

while (my $line = <$md_fh>) {
  $l++;
  if ($line =~ /^\s*$/) {
    $e++;
    next;
  }
  if ($line =~ /^#+\s*(.+)/) {
    print "$1\n";
    $h++; 
  }
}

print "\nS:\n"; 
print "L: $l\n";
print "E: $e\n"; 
print "H: $h\n";
```

You can use the following prompts to

1. `/explain` its purpose, and `/refactor` to improve the code.
2. `/refactor into Python` to get a working Python script.

![](assets/images/2024-07-duo-refactoring/pycharm_duo_refactor_perl_python.png)

> Tip: You can refactor Perl code into more target languages. The [GitLab Duo Coffee Chat: Challenge - Refactor Perl to Python](https://gitlab.com/gitlab-da/use-cases/ai/ai-workflows/gitlab-duo-challenges/code-challenges/challenge-refactor-perl-python) recording shows PHP, Ruby, Rust, Go, Java, VB.NET, C#, and more.

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/03HGhxXg9lw" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->



## More Refactoring Hands-on 

### Refactor JavaScript 

Eddie Jaoude shows how to refactor JavaScript to improve code quality, or add functionality in a practical example. 

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/mHn8KOzpPNY" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

### Refactor Bash to ZSH or SH

I have used Bash as shell for 20 years and most recently switched to ZSH on macOS. This resulted in script not working, or unknown errors in my terminal. Another use case for refactoring are shell limitations - some operating systems, Linux/Unix distributions do not provide Bash, only SH, for example, Alpine.

![](assets/images/2024-07-duo-refactoring/intellj_refactor_shell_scripts.png)

The [GitLab Duo Coffee Chat: Challenge - Refactor Shell Scripts](https://gitlab.com/gitlab-da/use-cases/ai/ai-workflows/gitlab-duo-challenges/code-challenges/challenge-refactor-shell-scripts) shows an example with a C program that can tail syslog files, and a build script written in Bash. Throughout the challenge, Duo Chat is queried with `/explain` and `/refactor` prompts to improve the code. It is also possible to refactor Bash into POSIX-compliant SH or ZSH. The session concludes with asking Duo Chat to provide 5 different Shell script implementations, and explain the key summaries. 


<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/mssqYjlKGzU" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

### More use cases and tutorials

- [Documentation: GitLab Duo use cases](https://docs.gitlab.com/ee/user/gitlab_duo/use_cases.html)
- [Tutorial blog: Top tips for efficient AI-powered code suggestions with GitLab Duo](https://about.gitlab.com/blog/2024/06/11/top-tips-for-efficient-ai-powered-code-suggestions-with-gitlab-duo/)
- [Tutorial blog: 10 best practices for using AI-powered GitLab Duo Chat](https://about.gitlab.com/blog/2024/04/02/10-best-practices-for-using-ai-powered-gitlab-duo-chat/)

## Key takeaways 

1. GitLab Duo provides efficient help with explaning and refactoring code. 
1. You can refactor code between language standards, and ask follow-up questions in Duo Chat.
1. Code Suggestions prompts can generate specific language standards, and code completion respects the current code context. 
1. Refactoring code into new programming languages helps with longer term migration and modernization plans.
1. Code can be "downgraded" into older system's supported language standards.
1. GitLab Duo can explain complex code and programming languages with different programming language examples.
1. The update to Claude 3.5 on GitLab.com has improved the quality and speed of Code Suggestions and Chat once again.
1. There are no boundaries except your imagination, and production pain points.

Learn more about efficient Code Suggestions and Chat workflows, and start your AI-powered code refactoring journey with GitLab Duo today!

<!-- Insert GitLab Duo Trial CTA here. -->

