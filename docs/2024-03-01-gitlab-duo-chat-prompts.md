---
Title: 10 tips for efficient AI-powered DevSecOps with GitLab Duo Chat
---

Getting into a conversation with AI can be challenging. How to start with a question? How to frame the question, how much context is needed? Which conversation provides the best and most efficient results?

In this tutorial, we explore ten tips and best practices to integrate GitLab Duo Chat into your AI-powered DevSecOps workflows.

Table of Content:

1. [Always in sight]()
1. [It is a conversation, not a search form]()
1. [Refine the prompt for more efficiency]()
1. [Prompt patterns]()
1. [Low context communication]()
1. [Repeat yourself]()
1. [Be patient]()
1. [Reset and start anew]()
1. [Efficiency with slash commands]()
1. [Get creative with slash commands]()
1. [Refine the prompt for slash commands]()
1. [Shortcuts]()
1. [Fun facts]()
1. [Learn more]()


### Always in sight

[GitLab Duo Chat](https://docs.gitlab.com/ee/user/gitlab_duo_chat.html) is available in the GitLab UI, Web IDE and supported programming IDEs, for example, VS Code. 

In VS Code, you can open GitLab Duo Chat in the default left pane. You can also drag and drop the icon into the right pane. This allows to keep Chat open while you write code and navigate the file tree, perform Git actions, etc. The following short video shows you how to do it.

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/foZpUvWPRJQ" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

The Web IDE and VS Code share the same framework -- the same method works in the Web IDE for more efficient workflows.

![](assets/images/2024-03-duo-chat-tips/gitlab_duo_chat_web_ide_right_pane.png)

### It is a conversation, not a search form

For the first conversation ice breaker, you can start with the same search terms similar to a browser search, and experiment with the response and output. In this example, lets try to start with a C# project and best practices. 

```markdown
c# start project best practices
```

![C# first question, short](assets/images/2024-03-duo-chat-tips/gitlab_duo_chat_vs_code_csharp_short_question_01.png)

The response is helpful to understand a broad scope of C#, but does not kickstart immediate best practices. Let us follow-up with more focussed question in the same context. 

```markdown
Please show the project structure for the C# project.
```

![C# second question, short](assets/images/2024-03-duo-chat-tips/gitlab_duo_chat_vs_code_csharp_short_question_02.png)

This answer is helpful. Next, follow-up with a Git question, and use the same question structure: Direct request to show something.

```markdown
Show an example for a .gitignore for C#
```

![](assets/images/2024-03-duo-chat-tips/gitlab_duo_chat_vs_code_csharp_short_question_03.png)

Continue with CI/CD, and ask how to build the C# project.

```markdown
Show a GitLab CI/CD configuration for building the C# project
```

![](assets/images/2024-03-duo-chat-tips/gitlab_duo_chat_vs_code_csharp_short_question_04.png)

In this example, Duo Chat encouraged us to request specific changes. Lets ask to use the .NET SDK 8.0 instead of 6.0. 

```markdown
In the above example, please use the .NET SDK 8.0 image
```

![](assets/images/2024-03-duo-chat-tips/gitlab_duo_chat_vs_code_csharp_short_question_05.png)

The CI/CD configuration uses the .NET CLI. Maybe we can use that for more efficient commands to create the projects and tests structure too? 

```
Explain how to create projects and test structure on the CLI 
```

![](assets/images/2024-03-duo-chat-tips/gitlab_duo_chat_vs_code_csharp_short_question_06.png)

Until now, all steps can be performed in the terminal, but how would that work in VS Code? Lets ask Duo Chat.

```markdown
Explain how to open a new terminal in VS Code
```

![](assets/images/2024-03-duo-chat-tips/gitlab_duo_chat_vs_code_csharp_short_question_07.png)

### Refine the prompt for more efficiency

**Tip:** Think of Duo Chat as a human, and engage with full sentences that provide as much context into your thoughts and questions. 

Experienced browser search users might know: Build up the question, add more terms to refine the scope, and restart the search after opening plenty of tabs. 

In a browser search, this probably would result in 4-5 different search windows. 

```markdown
c# start project best practices
c# .gitignore
c# gitlab cicd 
c# gitlab security scanning 
c# solutions and projects, application and tests
``` 

You can follow this strategy in a chat conversation, too. It requires adding more context, making it a conversational approach. One of the magic powers with GitLab Duo Chat is that you can ask multiple questions in one conversation request. Example: You need to start with a new C# project, apply best practices, add a `.gitignore` file, configure CI/CD and security scanning, just like we did before. In Duo Chat, you can phrase the questions into one request.

```markdown
How can I get started creating an empty C# console application in VSCode? Please show a .gitignore and .gitlab-ci.yml configuration with steps for C#, and add security scanning for GitLab. Explain how solutions and projects in C# work, and how to add a test project on the CLI.
```

![](assets/images/2024-03-duo-chat-tips/gitlab_duo_chat_prompt_refine_01.png)

In this response, GitLab Duo chat suggests to ask specific configuration examples in follow questions in the conversation. Async practice: Create follow-up questions. You can omit `C#` as context in the same chat session.

```markdown
Please show an example for a .gitignore
Please show a CI/CD configuration. Include the SAST template.
```


### Prompt patterns 

Follow the pattern `Problem statement, ask for help, provide additional requests.` Not everything comes to mind when asking the first question -- don't feel blocked, and instead start with `Problem statement, ask for help` in the first iteration. 

```markdown
I need to fulfill compliance requirements. How can I get started with Codeowners and approval rules?
```

![](assets/images/2024-03-duo-chat-tips/gitlab_duo_chat_prompt_patterns_01.png)

The answer is helpful, but lacks an example. You can either follow-up in the conversation with a new question, or refine the prompt. 

```markdown
Please show an example for Codeowners with different teams: backend, frontend, release managers.
```

![](assets/images/2024-03-duo-chat-tips/gitlab_duo_chat_prompt_patterns_02.png)

An alternative approach can be describing the situation you are in and ask for input. It can feel a bit like a conversation to follow the STAR model (Situation, Task, Action, Results). 

```markdown
I have a Kubernetes cluster integrated in GitLab. Please generate a Yaml configuration for a Kubernetes service deployment. Explain how GitOps works as a second step. How to verify the results?"
```

![](assets/images/2024-03-duo-chat-tips/gitlab_duo_chat_prompt_patterns_03.png)


### Low context communication 

Provide as much context as needed to provide an answer. Sometimes, the previous history or opened source code does not provide that helpful context. To make questions more efficient, apply a pattern of [low context communication](https://handbook.gitlab.com/handbook/company/culture/all-remote/effective-communication/#understanding-low-context-communication), used in all-remote commucatication at GitLab.

The following question did not provide enough context in a C++ project.

```markdown
Should I use virtual override instead of just override?
```

![](assets/images/2024-03-duo-chat-tips/gitlab_duo_chat_low_context_01.png)

Instead, try to add more context:

```markdown
When implementing a pure virtual function in an inherited class, should I use virtual function override, or just function override? Context is C++. 
```

![](assets/images/2024-03-duo-chat-tips/gitlab_duo_chat_low_context_02.png)
 
The example is also shown in the [GitLab Duo Coffee Chat: Refactor C++ functions into OOP classes for abstract database handling](https://youtu.be/Z9EJh0J9358?t=2190). 


### Repeat yourself

AI is not predictable. Sometimes, it may refuse to provide an answer, or does not include examples. It is recommended repeat the question, and optionally refine the requirements.

For example, when asking a refined question about C# projects, Chat responded with `I don't see how I can help. Please give better instructions!`. The instructions were clear, and I therefore copied the same text into the prompt again, leading to a more helpful response.

![](assets/images/2024-03-duo-chat-tips/gitlab_duo_chat_refuses_to_help_retry_01.png) 

Alternatively, reduce the scope of the question and ask about security and tests in follow 

```diff
-How can I get started creating an empty C# console application in VSCode? Please show a .gitignore and .gitlab-ci.yml configuration with steps for C#, and add security scanning for GitLab. Explain how solutions and projects in C# work, and how to add a test project on the CLI.
+How can I get started creating an empty C# console application in VSCode? Please show a .gitignore and .gitlab-ci.yml configuration with steps for C#.
```

When asking generic technology questions, GitLab Duo Chat might not be able to help. In the following scenario, I wanted to get a suggestion for a Java build tools and framework, and it did not work. There could be many answers: Maven, Gradle, etc. as build tools, and [100+ Java frameworks](https://en.wikipedia.org/wiki/List_of_Java_frameworks), depending on the technology stack and requirements.

![](assets/images/2024-03-duo-chat-tips/gitlab_duo_chat_refuses_to_help_retry_02.png) 

Lets assume that we want to focus on a customer environment with [Java Spring Boot](https://spring.io/projects/spring-boot). 

```markdown
I want to create a Java Spring Boot application. Please explain the project structure and show a hello world example.
```

![](assets/images/2024-03-duo-chat-tips/gitlab_duo_chat_refuses_to_help_retry_03.png) 

This provides great results already. As an async exercise, repeat the prompt, and ask how to deploy the application, adding more refinements in each step. Alternatively, make it a follow-up conversation.

```markdown
I want to create a Java Spring Boot application. Please explain the project structure and show a hello world example. Show how to build and deploy the application in CI/CD.

I want to create a Java Spring Boot application. Please explain the project structure and show a hello world example. Show how to build and deploy the application in CI/CD, using container images.

I want to create a Java Spring Boot application. Please explain the project structure and show a hello world example. Show how to build and deploy the application in CI/CD, using container images. Use Kubernetes and GitOps in GitLab.
```


### Be patient

Single words or short sentences might not generate the desired results, [shown in this video example](https://youtu.be/JketELxLNEw?t=1220). Sometimes, GitLab Duo Chat is able to guess from available data, but sometimes also might insist on providing more context.

Example: `labels` matches the GitLab documentation content.

![](assets/images/2024-03-duo-chat-tips/gitlab_duo_chat_patient_01.png)

Refine the question to problem statements and more refinements for issue board usage.

```markdown
Explain labels in GitLab. Provide an example for efficient usage with issue boards.
```

![](assets/images/2024-03-duo-chat-tips/gitlab_duo_chat_patient_02.png)

Or use a problem statement, followed by a question and the ask for additional examples.

```markdown
I don't know how to use labels in GitLab. Please provide examples, and how to use them for filters in different views. Explain these views with examples.
```

![](assets/images/2024-03-duo-chat-tips/gitlab_duo_chat_patient_03.png)

Also, avoid `yes/no` questions and instead add specific context.

```markdown
Can you help me fix performance regressions?
```

![](assets/images/2024-03-duo-chat-tips/gitlab_duo_chat_patient_04.png)

Instead, provide the context of the performance regression, including the programming languages, frameworks, technology stack and environments. The following example uses an environment from some years ago, which can still be accurate today.

```markdown
My PHP application encounters performance regressions using PHP 5.6 and MySQL 5.5. Please explain potential root causes, and how to address them. The app is deployed on Linux VMs.
```

![](assets/images/2024-03-duo-chat-tips/gitlab_duo_chat_patient_05.png)


### Reset and start anew

Sometimes, the chat history shows a different learning curve and provides the wrong context for follow-up questions. Or, you asked specific questions where GitLab Duo Chat cannot provide answers with. Since generative AI is not predictable, it might also lack providing certain examples, but think it gave them in a future response (observed in Duo Chat Beta). The underlaying Large Language Models sometimes might also insist on giving a specific response, in an endless loop.

```markdown
How can I get started creating an empty C# console application in VSCode? Please show a .gitignore and .gitlab-ci.yml configuration with steps for C#, and add security scanning for GitLab. Explain how solutions and projects in C# work, and how to add a test project on the CLI.
```

After asking the question above with an example configuration, I wanted to reduce the scope of the question to get a more tailored response. It did not work as expected, since Duo Chat knows about the chat history in context, and refers to previous answers.

```markdown 
How can I get started creating an empty C# console application in VSCode? Please show a .gitignore and .gitlab-ci.yml configuration with steps for C#.
```

![](assets/images/2024-03-duo-chat-tips/gitlab_duo_chat_reset_01.png)

To force Duo Chat into a new context, use `/reset` as slash command to reset the session, and repeat the question to get better results. You can also use `/clean` to delete all messages in the conversation.

### Efficiency with slash commands in the IDE 

#### Explain code

- Q: Generated code? Existing code? Legacy code?
- A: Use the [`/explain` slash command in the IDE](https://docs.gitlab.com/ee/user/gitlab_duo_chat.html#explain-code-in-the-ide).
- A2: Refine the prompt with more focussed responses, for example: `/explain focus on potential shortcomings or bugs`. 


![](assets/images/2024-03-duo-chat-tips/gitlab_duo_chat_slash_commands_explain_01.png)

![](assets/images/2024-03-duo-chat-tips/gitlab_duo_chat_slash_commands_explain_01.png)

#### Refactor code 

- Q: Unreadable code? Long spaghetti code? Zero test coverage?
- A: Use the [`/refactor` slash command in the IDE](https://docs.gitlab.com/ee/user/gitlab_duo_chat.html#refactor-code-in-the-ide). 
- A2: Refine the prompt for more targeted actions, for example object-oriented patterns: `/refactor into object-oriented classes with methods and attributes`. 

![](assets/images/2024-03-duo-chat-tips/gitlab_duo_chat_slash_commands_refactor_01.png)

![](assets/images/2024-03-duo-chat-tips/gitlab_duo_chat_slash_commands_refactor_02.png)

#### Generate tests

- Q: Testable code but writing tests takes too much time?
- A: Use the [`/tests` slash command in the IDE](https://docs.gitlab.com/ee/user/gitlab_duo_chat.html#write-tests-in-the-ide).
- A2: Refine the prompt for specific test frameworks, or test targets. You can also instruct the prompt to focus on refactoring, and then generate tests: `/tests focus on refactoring the code into functions, and generate tests`.

![](assets/images/2024-03-duo-chat-tips/gitlab_duo_chat_slash_commands_tests_01.png)

![](assets/images/2024-03-duo-chat-tips/gitlab_duo_chat_slash_commands_tests_02.png)

More practical examples in complete development workflows are available in the [GitLab Duo examples](https://docs.gitlab.com/ee/user/gitlab_duo_examples.html) documentation.

### Refine the prompt for slash commands 

You will see refined prompts tips in this blog post a lot. It is one of the ingredients for better AI-powered workflow efficiency. Slash commands are no different, and allow for better results in GitLab Duo Chat.

A customer recently asked: "Can code explanations using `/explain` create comments in code?" The answer is: no. But you can use the Duo Chat prompt to ask follow up questions, and ask for a summary in a code comment format. It requires the context of the language. 

The following example with a [C++ HTTP client code using the curl library](https://gitlab.com/gitlab-da/use-cases/ai/ai-workflows/gitlab-duo-prompts/-/blob/5cc9bdd65ee8ee16c548bea0402c18f8209d4d06/chat/slash-commands/c++/cli.cpp) needs more documentation. You can refine the `/explain` prompt by giving more refined instructions to explain the code by adding code comments, and then copy paste that into the editor.

```markdown
/explain add documentation, rewrite the code snippet
```

![](assets/images/2024-03-duo-chat-tips/gitlab_duo_chat_slash_commands_refine_prompt_01.png)

Alternatively, you can ask Duo Chat to `/refactor` the source code, and generate missing code comments through a refined prompt.

```markdown
/refactor add code comments and documentation
```

![](assets/images/2024-03-duo-chat-tips/gitlab_duo_chat_slash_commands_refine_prompt_02.png)

### Get creative with slash commands

When the Duo Chat prompt does not know an answer to a question about the source code or programming language, look into the slash commands `/explain`, `/refactor` and `/tests` and how much they can help in the context.

In the following example, an SQL query string in C++ is created in a single line. To increase readability, and also add more database columns in the future, it can be helpful to change the formatting into a multi-line string.

```cpp
std::string sql = "CREATE TABLE IF NOT EXISTS users (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT NOT NULL, email TEXT NOT NULL)";
```

You can ask GitLab Duo Chat about it, for example, with the following question:

```markdown
How to create a string in C++ using multiple lines?
```

![](assets/images/2024-03-duo-chat-tips/gitlab_duo_chat_creative_slash_commands_01.png) 

Chat may answer with an explanation and optional, a source code example. In this context, it interprets the question as an actual string which contains multiple lines. The requirement instead is to only write the code in multiple lines, but not the actual string. 

There is an alternative for additional context in VS Code and the Web IDE: Select the source code in question, right-click, and navigate into `GitLab Duo Chat > Refactor`. This opens the Duo Chat prompt and fires the `/refactor` code task immediately.

Although, the code task might not bring the expected the results. Refactoring a single line SQL string can mean a lot of things: Use multiple lines for readability, create constants, 

Code tasks provide an option to refine the prompt. You can add more text after the `/refactor` command, and instruct GitLab Duo Chat to use a specific code type, algorithm, or design pattern. 

Let us try it again: Select the source code, change focus into Chat, and type the following prompt, followed by `Enter`. 

```markdown
/refactor into a multi-line written string. Show different approaches for all C++ standards.
``` 

![](assets/images/2024-03-duo-chat-tips/gitlab_duo_chat_creative_slash_commands_02.png)


Tip: You can use GitLab Duo Code Suggestions to refine the source code even more after refactoring, or use alternative `/refactor` prompt refinements.

```markdown
/refactor into a multi-line written string, show different approaches
/refactor into multi-line string, not using raw string literals
/refactor into a multi-line written string. Make the table name parametrizable.
```

An alternative approach with the `stringstream` type is shown in the [GitLab Duo Coffee Chat: Refactor C++ functions into OOP classes for abstract database handling](https://www.youtube.com/watch?v=Z9EJh0J9358), [MR diff](https://gitlab.com/gitlab-da/use-cases/ai/gitlab-duo-coffee-chat/gitlab-duo-coffee-chat-2024-01-23/-/commit/7ea233138aed46d77e6ce0d930dd8e10560134eb#4ce01e4c84d4b62df8eed159c2db3768ad4ef8bf_33_35). 

#### Explain vulnerabilities

It might not always work, but the `/explain` slash command can be asked about security vulnerability explanation too. In this example, the [C code](https://gitlab.com/gitlab-da/use-cases/ai/ai-workflows/gitlab-duo-prompts/-/blob/5a5f293dfbfac7222ca4013d8f9ce9b462e4cd3a/chat/slash-commands/c/vuln.c) contains multiple vulnerabilities for strcpy() buffer overflows, world writable file permissions, race condition attacks and more.

```markdown
/explain why this code has multiple vulnerabilities
```

![](assets/images/2024-03-duo-chat-tips/gitlab_duo_chat_creative_slash_commands_03_vuln_explain.png) 

#### Refactor C code into Rust

Rust provides memory safety. You can ask Duo Chat to refactor the vulnerable [C code](https://gitlab.com/gitlab-da/use-cases/ai/ai-workflows/gitlab-duo-prompts/-/blob/5a5f293dfbfac7222ca4013d8f9ce9b462e4cd3a/chat/slash-commands/c/vuln.c) into Rust, using `/refactor into Rust`. Practice with more refined prompts to get better results.

```markdown
/refactor into Rust and use high level libraries
```

![](assets/images/2024-03-duo-chat-tips/gitlab_duo_chat_creative_slash_commands_04_refactor_c_to_rust.png) 


### Shortcuts 

Give them a try in your environment, and practice async using GitLab Duo Chat.

1. Inspect vulnerable code from CVEs, and ask what it does, and how to fix it, using `/explain why is this code vulnerable`. Tip: Import open-source projects in GitLab to take advantage of GitLab Duo Chat code explanations.
1. Try to refactor code into new programming languages to help legacy code migration plans.
1. You can also try to refactor Jenkins configuration into GitLab CI/CD, using `/refactor into GitLab CI/CD configuration`. 

### Fun facts 

Try to convince Duo Chat to behave like Clippy.

![](assets/images/2024-03-duo-chat-tips/gitlab_duo_chat_fun_prompts_01.png) 

Ask about GitLab's mission: "Everyone can contribute".

![](assets/images/2024-03-duo-chat-tips/gitlab_duo_chat_fun_prompts_02.png) 

### Learn more

There are many different environments and challenges out there. We have updated the [GitLab Duo Chat documentation](https://docs.gitlab.com/ee/user/gitlab_duo_chat.html) with more practical examples, and added a new [GitLab Duo examples](https://docs.gitlab.com/ee/user/gitlab_duo_examples.html) section with deep dives into AI-powered DevSecOps workflows, including Duo Chat.

Learning GitLab Duo works best through playful challenges, and real production code. The new learning series, GitLab Duo Coffee Chat, will continue in 2024. Until then, you can watch the recordings in [this YouTube playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Kp5uj_JgQiSvHw1jQu0mSVZ). If you are a GitLab customer, and interested in joining a GitLab Duo Coffee Chat to learn together, let me know in [this planning epic](https://gitlab.com/groups/gitlab-com/marketing/developer-relations/-/epics/476). 