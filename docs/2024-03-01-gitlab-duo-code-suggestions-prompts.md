---
Title: 20 tips for efficient AI-powered code suggestions with GitLab Duo
---


AI-powered DevSecOps workflows with GitLab Duo require hands-on practice, and learning in public together. This blog tutorial explains tips and tricks, learned best practices, and some hidden gems on combining all GitLab Duo features to get more efficient in your DevSecOps workflows. It also highlights how AI greatly improves the developer experience.

Everything you will read and learn here has been created "from scratch", has or will be added the [GitLab Duo documentation](https://docs.gitlab.com/ee/user/ai_features.html), and [GitLab Duo prompts project](https://gitlab.com/gitlab-da/use-cases/ai/ai-workflows/gitlab-duo-prompts), maintained by the Developer Relations team. Bookmark this blog tutorial, and navigate into the respective chapters individually. All tips and stories will aid your learning experience individually. 

Table of Content:

1. [Why Code Suggestions?]()

1. [Start simple, refine prompts]()
1. [Practice, practice, practice]()
    - [Fix missing dependencies]()
    - [Boilerplate code: Optimized logging]()
    - [Generate regular expressions]()
1. [Re-trigger Code Suggestions]()
    - [Common keyboard combinations to re-trigger]()
    - [Stuck in the middle of suggestions]()
    - [Code Suggestions stopped]()
1. [Code Suggestions vs Code Generation]()
    - [Start with a comment on top for code generation]()
    - [Intent detection for suggestions and generation]()
    - [Tell a story]()
    - [Generate regular expressions]()
1. [Advanced prompt engineering with Code Generation]()
    - [Iterate faster with Code Generation]()
    - [Practical Code Generation: Cloud-Native Observability]()
1. [Take advantage of all GitLab Duo features]()
    - [Combine Chat with Code Suggestions]()
    - [Use Chat to generate build configuration]()
    - [Use Chat to explain potential vulnerabilities]()
    - [Combine Vulnerability Resolution with Code Suggestions]()
1. [More Tips]()
    - [Verify code quality and security]()
    - [Learn as a team, and understand AI impact]()    
    - [Development is a marathon, not a sprint]()
    - [Contribute using GitLab Duo]()


1. [Conclusion]()


## Why Code Suggestions?

Think about the following scenarios:

1. As a senior developer, you feel confident in designing and implementing robust architectures. Learning the latest programming language features requires time, research, and change of habits.

> "Learning the latest and greatest language features takes time. As an experienced developer, I have my settled language set that helps write code fast, and build reliable architecture. But how would I learn about new language feature additions, that make the code even more robust, or use resources more sustainably?"

As a personal example, I learned C++03, later C++11 and never really touched base on C++14/17/20/23 standards. Additionally, new languages such as [Rust](https://about.gitlab.com/blog/2023/08/10/learning-rust-with-a-little-help-from-ai-code-suggestions-getting-started/) come around and offer better developer experience. What now?

2. As a beginner in development, it is hard to understand specific algorithms, and know the documentation for structures, interfaces, etc. Learning under pressure and with many roadblocks ahead can lead to errors. 

> "In a similar thought process, as a beginner in a new language, I will need to invest into learning. While developing new features under deliverables pressure, there might be mistakes, too, and no time for learning best practices."

As a personal example, I never really learned frontend engineering, just some self-taught HTML, CSS and JavaScript. Adapting into frontend frameworks such as VueJS after a decade feels overwhelming, and I have little time to learn.

## Start simple, refine prompts

My own GitLab Duo adoption journey started with single line code comments, leading to not so great results at first. 

```
# Generate a webserver

// Create a database backend

/* Use multi-threaded data access here */
```

After experimenting with different contexts, and writing styles, code generation out of comments worked better. 

```
# Generate a webserver, using the Flask framework. Implement the / URL endpoint with example output.

// Create a database backend. Abstract data handlers and SQL queries into function calls.

/* Use multi-threaded data access here. Create a shared locked resource, and focus on supporting Linux pthreads. */
```

Code comments alone won't do the trick, though. Lets explore more best practices.

## Practice, practice, practice 

Find use cases and challenges for your daily workflows, and exclusively use GitLab Duo. It can be tempting to open browser search tabs, but can you also just use GitLab Duo to solve the challenge in your IDE?

1. Fix missing dependencies (which always cause build/execution failures).
1. Missing logging context, let Code Suggestions auto-complete started function calls including `print` statements.
1. Generate common methods and attributes for object-oriented design patterns (e.g. getter/setter methods, `toString()` and object comparison operators, object inheritance, etc.).
1. Function generates random crashes. Use Code Suggestions to implement a new function which implements a different algorithm.
1. Application cannot be compiled or executed, cryptic error (Tip: Ask Duo Chat about it.)
1. Learn about existing (legacy) code, and strategies to document and refactor code into modern libraries. Start a v2 of an application with a new framework or programming language, helping solve technical debt.
1. Understand and embrace Ops and Sec problems before they are committed to Git (performance, crashes, security vulnerabilities, etc.).

Think of the most boring - or most hated - coding task, and add it to the list above. Mine are definitely attribute getter/setter methods in C++ classes, as can be seen in the [GitLab Duo Coffee chat recording on YouTube](https://www.youtube.com/watch?v=Z9EJh0J9358). C++ matches itself with Java and C#, though.

It can also help to use Code Suggestions in different programming languages, for example focussing on backend and frontend languages. If you are experienced in many languages, take a look into languages that you have not used in a while, or look into learning a new programming language, for example [Python](https://about.gitlab.com/blog/2023/11/09/learning-python-with-a-little-help-from-ai-code-suggestions/) or [Rust](https://about.gitlab.com/blog/2023/08/10/learning-rust-with-a-little-help-from-ai-code-suggestions-getting-started/). 

Adapting Code Suggestions into fast auto-completion workflows can happen transparently, too. The greyed out suggestion is optional (depends on the UI, for example, VS Code), and should not distract you from continuing to write source code. Especially in the beginning, the suggestions can feel irritating, but after practice, they are "just" optional help, and can be skipped when not needed.

![](assets/images/2024-03-duo-code-suggestions-tips/duo_code_suggestions_java_springboot_class_methods_tostring.png)


### Fix missing dependencies

After building or running source code, missing dependency errors might be logged and prevent further execution and testing. The following example in Go shows an error from `go build`, where the source code did not import any dependencies yet. A manual approach can be collecting all listed dependencies, run a unique sort on them, and add them into the source code file.

![](assets/images/2024-03-duo-code-suggestions-tips/duo_code_suggestions_go_build_failed_missing_deps.png)

But what if GitLab Duo knows about the file context and missing dependencies already? Navigate into the top section and add a comment, saying `// add missing imports` and wait for Duo Code Suggestions.

![](assets/images/2024-03-duo-code-suggestions-tips/duo_code_suggestions_go_build_failed_missing_deps_suggested_fix.png)


```golang
// add missing imports

import (
    "fmt"
    "strings"
)
```

Running `go build` again results in success, and the source code can be tested and run.

![](assets/images/2024-03-duo-code-suggestions-tips/duo_code_suggestions_go_build_failed_missing_deps_fixed.png)

### Boilerplate code: Optimized logging

Q: Logging -- and more Observability data with metrics and traces -- can be hard and tedius to implement. What is the most efficient way, which does not impact the application performance or cause bugs?
A: Use Code Suggestions to generate logging function calls, and refactor the code into robust observability instrumentation library abstractions. This method can help to prepare the code for later integration with OpenTelemetry, for example.

Example for a Logging class in Ruby:

```ruby
# Create Logging utility class
# Define default log level as attribute
# Add method for logging, inputs: level, app, message
# Print the data with formatted date and time in syslog format

# Potential AI-generated code below
class Logging
  attr_accessor :log_level

  def log(level, app, message)
    time = Time.now.strftime("%b %d %H:%M:%S")
    puts "#{time} #{app}[#{Process.pid}]: #{level} - #{message}"
  end
end

# Instantiate class and test the log method

# Potential AI-generated code below
logger = Logging.new
logger.log_level = :debug

logger.log(:info, 'MyApp', 'Application started - info')
logger.log(:debug, 'MyApp', 'Application started - debug')
logger.log(:error, 'MyApp', 'Application started - error')
logger.log(:fatal, 'MyApp', 'Application started - fatal')
logger.log(:warn, 'MyApp', 'Application started - warn')
```

### Utility helper functions, well tested

Q: The programming language does not provide basic functions in the standard library. I'm tempted to open my browser search to add string manipulation, and regular expression parser functions.
A: Create a new file called `utility.{language-suffix}` and add a code comment on top. Instruct Code Suggestions to generate a string manipulation function ()

Example for a string manipulation method in Python:

```python
# Create a function to search for a string in a string, return the index
# Optionally remove search string, and return modified string
# Test the function on the string "Cool programming languages: C++, Rust, Python" - Remove the string "C++"

# Potential AI-generated code below
def search_and_remove(text, search):
    index = text.find(search)
    if index != -1:
        return index, text[:index] + text[index+len(search):]
    else:
        return None, text

text = "Cool programming languages: C++, Rust, Python"
index, new_text = search_and_remove(text, "C++")
print(index, new_text)
```

**Async practice:** Ask Duo Chat how to add tests in Python, select the source code and use the `/tests` slash command. 

A similar example can be implementing in Go, creating utility functions for unique integer values in an array, or the sum of all evi... even. Start with defining the project structure through `go mod init gitlab.com/group/project` and create the `array.go` file. Define the `package` and start with the first code comment to generate the functions.

```go
package array 

// Create a function that returns unique values from an integer array

// Possible AI-generated code
func unique(ints []int) []int {
    occurred := map[int]bool{}
    result := []int{}
    for _, i := range ints {
        if !occurred[i] {
            occurred[i] = true
            result = append(result, i)
        }
    }
    return result
}

// Create a function that returns the sum of all even numbers in an integer array

// Possible AI-generated code
func sumEvens(ints []int) int {
    var sum int
    for _, i := range ints {
        if i%2 == 0 {
            sum += i
        }
    }
    return sum
}
```

**Async exercise**: Create more utility helper functions in dedicated libraries, and use Duo Chat to select and generate `/tests`. For the Go example, you can inspect potential solutions in the `go/utility/array_test.go` file in the [GitLab Duo Prompts project](https://gitlab.com/gitlab-da/use-cases/ai/ai-workflows/gitlab-duo-prompts). Build and test the code using `go build && go test`.

### Generate regular expressions

Developer's favorite one liners, never touched again. `git blame` knows very well. But GitLab Duo can at least help with the initial creation, and refactoring, in the following example:

Q: My regular expressions for parsing IPv6 and IPv4 addresses do not work. What's the best approach to solve this?
A: Use Code Suggestions comments to generate examples using these regex types. Combine the questions with Duo Chat, and ask for more examples in different languages. You can also select the existing source, and use a refined prompt with `/refactor using regular expressions` in the Chat prompt.

**Async exercise**: Choose your favorite language, create a function stub that checks IPv6 and IPv4 address strings for their valid format. Trigger Code Suggestions to generate a parsing regular expression code for you. Optionally ask GitLab Duo Chat how to refine and refactor the regex for greater performance.

I chose TypeScript, a language on my personal learning list for 2024: `// Generate a TypeScript function which parses IPv6 and IPv4 address formats. Use regular expressions`.

![](assets/images/2024-03-duo-code-suggestions-tips/duo_code_suggestions_typescript_utility_parse_ip_address_regex.png)

![](assets/images/2024-03-duo-code-suggestions-tips/duo_code_suggestions_typescript_utility_parse_ip_address_regex_tests.png)


## Re-trigger Code Suggestions

You can trigger Code Suggestions by pressing the `enter` or `space` key, depending on the context. In VS Code and the GitLab Web IDE, the GitLab Duo icon will appear in the same line, and at the bottom of the window.

If you accepted a suggestion, but actually want to try a different suggestion path, select the code, delete the line(s) and start over.

> **Tip:** Different keystrokes and strategies for Code Suggestions are recorded in the [GitLab Duo Coffee Chat: Code Generation Challenge with Go and Kubernetes Observability](https://www.youtube.com/watch?v=ORpRqp-A9hQ) recording.

### Common keyboard combinations to re-trigger

Especially in the early adoption phasis of GitLab Duo Code Suggestions, you'll need to practice to get the best results from comments, existing code style, etc. put into context.

A common key stroke pattern for triggering suggestions can be

1. Press `Enter` and wait for the suggestion.
1. Press `Space` followed by `Backspace` to immediately delete the whitespace again, or
1. Press `Enter` to re-trigger the suggestion. `Backspace` to delete any left over new lines.

When a suggestion makes sense, or you want to see how far you can get:

1. Continue pressing `Tab` to accept the suggestion.
1. Add a space or press `Enter` to open a new scope for triggering a new suggestion.
1. Continue accepting suggestions with `Tab`. 

Note that generative AI sometimes ends up in a loop of suggesting similar code paths over and over again. You can trigger this behavior with inserting test data into an array, using strings and numbers in a sorted order. Or by generating different API endpoints - it tries to guess which other endpoints could be helpful. When this happens, break the acceptance flow, and continue writing code as normal.

### Stuck in the middle of suggestions

Sometimes, the code suggestions may stop in the middle of a variable, function, etc. definition. If you are unsure about the syntax, or want to restart Code Suggestions otherwise:

1. Delete the last character(s) or the entire line, using `Backspace`.
1. Alternatively, use `shift cursor left` (select characters) or `cmd shift cursor left` (select entire line), followed by `Backspace`.
1. Move the cursor into the line above, and press `Enter` to force a Code Suggestions trigger again.

### Code Suggestions stopped

When Code Suggestions stop, it can have multiple reasons:

1. The current file scope ends, for example, a `main()` function has been generated and closed.
1. Connection problems to the GitLab instance (self-managed) or GitLab.com (SaaS, [Dedicated](https://about.gitlab.com/dedicated/)). Follow the [troubleshooting documentation](https://docs.gitlab.com/ee/user/project/repository/code_suggestions/troubleshooting.html).

## Code Suggestions vs Code Generation

Code Suggestions "come as you go" while writing code, and help with completing line(s). Code generation on the other hand requires more context to create entire code blocks, consisting of functions, algorithms, classes, etc. 

The following sections discuss both methods, and how to get started with a practical example. 

### Code Suggestions flow with comments

Use your natural programming flow, and stop to adapt to adding code comments when helpful for context and better suggestions. You can accept Code Suggestions using the `Tab` key, or selectively accept words using the `cmd cursor right` keyboard shortcut.

The following new challenge implements a simple Linux statistics tool in C, mimicking the functionality of `iostat`, `vmstat` and `du` CLI commands on Linux. Sometimes, these low level metrics come in handy for presenting application metrics, or otherwise help with debugging when requesting support data from customers.

Create a new application directory called `linux_stat` and `main.c` file, and let Duo Code Suggestions complete everything else. First, start with a simple code comment block:

```c
// Create a Linux statistic tool
// Collect IO, performance, disk usage, TCP latency
// Print summaries
```

Next, press `Enter` to trigger the creation of the `main()` method. If not automatically created, force it through a new code comment.

```c
// Create main function to collect data
```

In my attempts, generative AI did not immediately implement all functionality. Instead, it added a `TODO` comment into the following code block.

```c
int main() {
    struct stat statbuf;

    if (stat("/etc", &statbuf) != 0) {
        perror("stat");
        return 1;
    }

    printf("IO Block Size: %ld\n", statbuf.st_blksize);

    // TODO: add CPU usage, disk usage, network latency measurements

    return 0;
}
```

Move the cursor after the `TODO` comment, and continue with a new comment to collect CPU usage:

```c
    // Collect CPU usage

    // Example for AI-generated code below
    double cpu_usage;

    FILE* stat_file = fopen("/proc/stat", "r");
    if (stat_file == NULL) {
        perror("fopen");
        return 1;
    }

    // Parse /proc/stat to get CPU usage
    fscanf(stat_file, "cpu %lf", &cpu_usage);

    printf("CPU Usage: %.2f%%\n", cpu_usage * 100);

    fclose(stat_file);
```

The generated code might not be 100% accurate, since measuring CPU usage on Linux requires to take samples over a defined time range. Thus, the caller needs to calculate the real CPU utilization from all values in `/proc/stat`, including [`system, nice, user, idle, iowait`, etc.](https://www.kernel.org/doc/Documentation/filesystems/proc.txt). Async practice: Optimize the collection process with average calculation.

Repeat the steps for disk usage and network latency. The code comment is written by you, the C source code generated with GitLab Duo.

```c
    // Collect disk usage

    // Collect network latency

    // Example for AI-generated code below
    int sockfd = socket(AF_INET, SOCK_STREAM, 0);

    struct sockaddr_in servaddr;
    servaddr.sin_family = AF_INET;
    servaddr.sin_port = htons(80);
    inet_pton(AF_INET, "8.8.8.8", &servaddr.sin_addr);
```

The generated code may use public IP addresses to test network latency. Change the IPv4 address to GitLab.com's current address. 

```c
    inet_pton(AF_INET, "172.65.251.78", &servaddr.sin_addr);
```

The generated source code added more header dependencies. Navigate into the top section of the file, and add a comment to include all required headers:

```c
// Import required headers
```

Build and run the application on Linux, example on Ubuntu 22 LTS:

```shell
# Install compiler
apt install gcc

# Build
gcc main.c -o linux_stat

# Build: If the math.h header is included, linking against the library is needed, too
gcc linux_stat.c -o linux_stat -lm

# Run the tool
./linux_stat
```

Bonus: Change the code to use IPv6, and check the GitLab.com IPv6 address again (`dig gitlab.com AAAA +short`).

```c
    // Collect network latency

    // Use IPv6 

    // Example for AI-generated code below
    struct sockaddr_in6 servaddr;
    servaddr.sin6_family = AF_INET6;
    servaddr.sin6_port = htons(80);
    //inet_pton(AF_INET6, "2001:db8::1", &servaddr.sin6_addr);
    inet_pton(AF_INET6, "2606:4700:90:0:f22e:fbec:5bed:a9b9", &servaddr.sin6_addr);

    int sockfd = socket(AF_INET6, SOCK_STREAM, 0);
```

![](assets/images/2024-03-duo-code-suggestions-tips/duo_code_suggestions_c_linux_stat_tests.png)

The full working source code is avaible in the [GitLab Duo Prompts project](https://gitlab.com/gitlab-da/use-cases/ai/ai-workflows/gitlab-duo-prompts) in the directory for C code.

**Async challenge:** Refactor the C code into Rust, using only GitLab Duo. Start by selecting the source code, and use the Duo Chat prompt `/refactor into Rust`. 

> Tip: Thoughtful code comments make the source code more readable, too. This helps new team members with onboarding, SREs with debugging production incidents, and Open Source contributors with [their first MR merged](https://handbook.gitlab.com/handbook/marketing/developer-relations/contributor-success/community-contributors-workflows/#first-time-contributors).



### Start with a comment on top for code generation

Source code can be organized in multiple files. Whether you start with a new application architecture, or refactor existing source code, you can take advantage of Code Generation with GitLab Duo.

Start with a comment block on top, and make it a step-by-step description. You can also break longer comments into multiple lines, revisiting the examples in this blog post. This pattern also helps to think about the requirements, and can help refining the prompts. 

```diff
# Generate a webserver, using the Flask framework. 
# Implement the / URL endpoint with example output.
+# Add an endpoint for Promtheus metrics

// Create a database backend. 
// Abstract data handlers and SQL queries into function calls.
+// Use PostgreSQL as default backend, and SQLite for developers as fallback.

/* 
Use multi-threaded data access here.
Create a shared locked resource, and focus on supporting Linux pthreads. 
+Abstract the thread creation/wait procedures into object-oriented classes and methods.
*/
```

More Code Generation prompts for [supported programming languages](https://docs.gitlab.com/ee/user/project/repository/code_suggestions/supported_extensions.html) are available in the [GitLab Duo Examples documentation](https://docs.gitlab.com/ee/user/gitlab_duo_examples.html#code-generation-prompts).

### Intent detection for suggestions and generation

Code Suggestions, respectively the GitLab Language Server in your IDE, parse and detect the intent and offer code completion suggestions in the same line, or code generation. The technology in the background uses TreeSitter to parse the code into an AST, and determine whether the scope is inside a code comment (block), or inside the source code. This needs to be executed fast on the client IDE, and proves to be a great use case for WebAssembly. You can learn more in [this epic](https://gitlab.com/groups/gitlab-org/-/epics/11568), and the recording of the GitLab Duo Coffee Chat: An inside look into the GitLab Language Server powering Code Suggestions:

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/VQlWz6GZhrs" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

### Tell a story for efficient code generation

Code generation is art. Tell the story, and AI-powered GitLab Duo may assist you. 

The following example aims to implement an in-memory key-value store in Go, similar to Redis. Start with a description comment, and trigger Code Suggestions by continueing with a new line by pressing `Enter`.

```golang
// Create an in-memory key value store, similar to Redis 
// Provide methods to
// set/unset keys
// update values
// list/print with filters
```

We can be more specific -- which methods are required for data manipulation? Instruct Code Suggestions to generate methods for setting keys, updating values, and list all contained data.

```golang
// Create an in-memory key value store, similar to Redis 
// Provide methods to
// set/unset keys
// update values
// list/print with filters
```

Accept all suggestions using the `Tab` key. As a next step, instruct Code Suggestions to create a `main` function with test code.

```golang
// Create a main function and show how the code works
```

If the test data is not enough, refine the generated code with a focus on extreme test cases. Tip: You can use the same method for refined [Duo Chat prompts and test generation](https://docs.gitlab.com/ee/user/gitlab_duo_chat.html#write-tests-in-the-ide), `/tests focus on extreme test cases`.

```golang
// Add more random test data, focus on extreme test cases
```

![](assets/images/2024-03-duo-code-suggestions-tips/duo_code_suggestions_go_kv_more_test_data.png)

The full example, including fixed dependencies (see above), is located in the [gitlab-duo-prompts project](https://gitlab.com/gitlab-da/use-cases/ai/ai-workflows/gitlab-duo-prompts) in the `code-suggestions/go/key-value-store` directory. Update the `main.go` file, and build and run the code using the following command: 

```shell
go build
./key-value-store
```

The first iteration was to create a standalone binary and test different implementation strategies for key-value stores. Commit the working code, and continue with your GitLab Duo adoption journey in the next step.

> Tip: New projects can benefit from Code Generation, and require practice and more advanced techniques to use code comments for prompt engineering. This method can also make experienced development workflows more efficient. Proof of concepts, new library introductions, or otherwise fresh iterations might not always be possible in the existing project and framework. Experienced developers seek to create temporary projects, and isolate or scope down the functionality. For example, introducing a database backend layer, and benchmarking it for production performance. Or, a library causing security vulnerabilities or license incompatibilities should be replaced with a different library, or embedded code functionality.

### Iterate faster with Code Generation

Experienced developers will say - "there must be a key-value library in Go, let us not reinvent the wheel." Fortunately, Go is a mature language with a rich ecosystem, and awesome-go collectoon projects, for example [avelino/awesome-go](https://github.com/avelino/awesome-go), provide plenty of example libraries. Note: This possibility might not be the case for other programming languages, and requires a case-by-case review.

We can also ask GitLab Duo Chat first, `Which Go libraries can I use for key-value storage?`:

![](assets/images/2024-03-duo-code-suggestions-tips/duo_chat_ask_golang_libs_kv.png)

And then refine the Code Suggestions prompt to specifically use the suggested libraries, for example BoltDB.

```diff
// Create an in-memory key value store, similar to Redis 
// Provide methods to
// set/unset keys
// update values
// list/print with filters
+// Use BoltDB as external library
```

Repeat the pattern from above: Generate the source code functions, then ask Duo to create a main function with test data, and build the code. The main difference are external libraries which need to be pulled with the `go get` command first. 

```shell
go get
go build
```

If the source code build fails with missing dependencies such as `fmt`, practice using Duo again: Move the cursor into the `import` statement, and wait for the suggestion to add the missing dependencies. Alternatively, add a comment saying `Import all libraries`.

![](assets/images/2024-03-duo-code-suggestions-tips/duo_code_suggestions_go_kv_external_lib_boltdb_fix_deps.png)

You can also add more test data again, and verify how the functions behave: `// Add more random test data, focus on extreme test cases`. In the following example, an empty key causes the program to panic.

![](assets/images/2024-03-duo-code-suggestions-tips/duo_code_suggestions_go_kv_external_lib_boltdb_test_extreme_cases_panic.png)

This example is a great preparation for test cases later on.

### Practical Code Generation: Cloud-Native Observability

Think of a client application in Go, which lists the current state of containers, pods and services in a Kubernetes cluster, similar to the `kubectl get pods` command line. The Kubernetes project provides [Go libraries](https://pkg.go.dev/k8s.io/client-go/kubernetes) to programmatically interact with the Kubernetes APIs, interfaces and object structures.

Open your IDE, and create a new Go project. Tip: you can ask GitLab Duo Chat how to do it: `How to start a Go project? Please show CLI command examples`. 

Start with a single comment on top of the `main.go` file, and describe the application purpose: Observability in Kubernetes.

```golang
// Create a client for Kubernetes observability
```

Think about the main requirements: Get access to Kubernetes, create context, namespace and inspect the state. Additionally, instruct Code Suggestions to import packages and and create a main package in the `main.go` file.

First iteration:

```golang
// Create a client for Kubernetes observability
// Inspect container, pod, service status and print an overview
```

Might do unexpected things with hardcoding the access credentials, missing contexts, failing builds.

Second iteration:

```golang
// Create a client for Kubernetes observability
// Create a function that
// Read the kubernetes configuration file from the KUBECONFIG env var
// Inspect container, pod, service status and print an overview
```

Might not know about Kubernetes contexts and namespaces, thus leading to build errors or unexpected results.

Third iteration:

```golang
// Create a client for Kubernetes observability
// Create a function that
// Read the kubernetes configuration file from the KUBECONFIG env var
// Create kubernetes context, namespace default
// Inspect container, pod, service status and print an overview
```

This example hardcodes the Kubernetes context and default namespace to generate an initial foundation. Later iterations can read the namespace value from a command line parameter, or configuration file.

The final example can look like the following. In addition to the application functionality, it also instructs Code Suggestions to import all dependencies, and create a `main` package in `main.go`.

```golang
// Create a client for Kubernetes observability
// Create a function that
// Read the kubernetes configuration file from the KUBECONFIG env var
// Create kubernetes context, namespace default
// Inspect container, pod, service status and print an overview
// Import necessary packages
// Create main package
```

<details> Solution 
<summary>

```golang
package main

import (
	"context"
	"fmt"
	"os"

	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/clientcmd"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func main() {
	kubeconfig := os.Getenv("KUBECONFIG")
	config, err := clientcmd.BuildConfigFromFlags("", kubeconfig)
	if err != nil {
		panic(err)
	}

	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		panic(err)
	}

	pods, err := clientset.CoreV1().Pods("").List(context.TODO(), metav1.ListOptions{})
	if err != nil {
		panic(err)
	}
	fmt.Printf("There are %d pods in the cluster\n", len(pods.Items))

	// Additional code to inspect services, containers, etc
}
```

</summary>
</details>

Example output:

![](assets/images/2024-03-duo-code-suggestions-tips/duo_code_suggestions_go_k8s_o11y_output.png)


**Async exercise:** Complete the project with code for inspecting services, containers, etc. and export the findings to [OpenTelemetry](https://opentelemetry.io/).

> Tip: Practice with the [GitLab Duo Examples: Code generation prompts](https://docs.gitlab.com/ee/user/gitlab_duo_examples.html#code-generation-prompts) in the documentation, and/or send merge requests with your working prompts.

While recording a short video to show that Code Generation is working, another better code was generated. You can inspect the differences in [this commit](https://gitlab.com/gitlab-da/use-cases/ai/ai-workflows/gitlab-duo-prompts/-/commit/a1a46de9789d4791f04b4df9f1a35d05b8e67568), and benefit from both solutions.

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/ORpRqp-A9hQ" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->


## Take advantage of all GitLab Duo features 

### Combine Chat with Code Suggestions

In combination with [Duo Chat](https://docs.gitlab.com/ee/user/gitlab_duo_chat.html), Code Suggestions become even more powerful. The following workflow illustrates the intersection of AI efficiency:

Write and generate new code using Duo Code Suggestions. The source code will be verified through CI/CD automation, code quality tests, and security scanning. But what about the developer's knowledge?

A: In your IDE, select the generated code portions and use the [`/explain` slash command](https://docs.gitlab.com/ee/user/gitlab_duo_chat.html#explain-code-in-the-ide) in the Chat prompt. You can even refine the prompt to `/explain with focus on algorithms`, or otherwise helpful scopes such as potential security or performance problems, etc.

Continue writing and maintaining source code, but at some point code quality decreases and refactoring gets challenging. Ask GitLab Duo Chat for help.

A: In your IDE, select the source code, and use the [`/refactor` slash command](https://docs.gitlab.com/ee/user/gitlab_duo_chat.html#refactor-code-in-the-ide) in the Chat prompt. You can refine the prompt to focus on specific design patterns (functions, object-oriented classes, etc.), `/refactor into testable functions` for example.

After ensuring more readable code, tests need to be written. What are potential extreme cases, or random data examples for unit tests? Research and implementation in various frameworks can take time.

A: In our IDE, select the source code, and use the [`/tests` slash command](https://docs.gitlab.com/ee/user/gitlab_duo_chat.html#write-tests-in-the-ide) in the Chat prompt. You can also refine the prompt to focus in specific test frameworks, scenarios, input methods, etc. 

Code quality and test coverage reports are green again. Focus on efficient DevSecOps workflows with Code Suggestions again. 

More scenarios are described in the [GitLab Duo examples documentation](https://docs.gitlab.com/ee/user/gitlab_duo_examples.html).

### Use Chat to generate build configuration

The time intense research on getting started with a new project can be exhausting. Especially with different paths to do it right, or alternative frameworks, this can lead to more work than anticipated. Newer programming languages like Rust propose one way (Cargo), while Java, C++, etc. offer multiple ways and additional configuration languages on top (Kotlin DSL, CMake DSL, etc.).

Take advantage of asking GitLab Duo how to start a project, generate specific configuration examples for build tools (e.g. `Please show a gradle.build example for Spring Boot`), and reduce the time to start developing, building and testing source code.

1. Java, Gradle, Spring Boot: `Please show a gradle.build example for Spring Boot`
1. C++, CMake, clang: `Please show a basic CMake configuration file for C++17, using clang as compiler.`
1. Python: `Please show how to initialize and configure a Python project on the CLI`
1. Rust: `Please show how to initialize and configure a Rust project.`, followed by a refinement question: `Explain the structure of Cargo.toml`.
1. Go: `Please show how to initialize and configure a Go project`. 

### Use Chat to explain potential vulnerabilities

Let us assume that some PHP code was generated to create a web form. The code might be vulnerable to security issues.

```php
<?php 
// Create a feedback form for user name, email, and comments
// Render a HTML form

$name = $_POST['name'];
$email = $_POST['email'];
$comments = $_POST['comments'];

echo '<form method="post">';
echo '<label for="name">Name:</label>';
echo '<input type="text" id="name" name="name">';

echo '<label for="email">Email:</label>';
echo '<input type="email" id="email" name="email">';

echo '<label for="comments">Comments:</label>';
echo '<textarea id="comments" name="comments"></textarea>';

echo '<input type="submit" value="Submit">'; 
echo '</form>';

?>
```

Select the source code, and [ask Duo Chat to explain](https://docs.gitlab.com/ee/user/gitlab_duo_chat.html#explain-code-in-the-ide), using a refined prompt with `/explain why this code is vulnerable to bad security actors`. 

![](assets/images/2024-03-duo-code-suggestions-tips/duo_code_suggestions_chat_explain_potential_vulnerability.png)


> **Tip**: We are investigating and learning in the local developer's environment. The vulnerable source code can be fixed before it reaches a git push, and Merge Request that trigger security scanning, which will unveil and track the problems, too. 
> 
> Learning about security vulnerabilities helps improve the developer experience.

### Combine Vulnerability Resolution with Code Suggestions

Lets look into another example with an intentional [vulnerability resolution challenge](https://gitlab.com/gitlab-da/use-cases/ai/ai-workflows/gitlab-duo-challenges/vulnerability-resolution/challenge-resolve-vulnerabilities), and see if we can use Code Suggestions in combination with Vulnerability Resolution. The linked project has been preconfigured with SAST scanning. You can follow these steps to configure GitLab SAST by using the [SAST CI/CD component](https://gitlab.com/explore/catalog/components/sast) in the `.gitlab-ci.yml` CI/CD configuration file.

```yaml
include:
  # Security: SAST (for vulnerability resolution)
  - component: gitlab.com/components/sast/sast@1.1.0
```

After inspecting the vulnerability dashboard and details, you can use [Vulnerability Explanation](https://docs.gitlab.com/ee/user/application_security/vulnerabilities/#vulnerability-explanation) to better understand the context and potential problems. [Vulnerability Resolution](https://docs.gitlab.com/ee/user/application_security/vulnerabilities/index.html#vulnerability-resolution) creates a Merge Request with a proposed source code fix for a detected security vulnerability. 


Sometimes, it can be necessary to refine the suggested code. Navigate into the [created MR](https://gitlab.com/gitlab-da/use-cases/ai/ai-workflows/gitlab-duo-challenges/vulnerability-resolution/challenge-resolve-vulnerabilities/-/merge_requests/1), and either copy the Git branch path for local Git fetch, or open the Web IDE from the `Edit` button to continue in the browser. Navigate into the source code sections with the fixed code portions, and modify the code with a comment:

```
// refactor using safe buffers, null byte termination
```

![](assets/images/2024-03-duo-code-suggestions-tips/duo_code_suggestions_with_vulnerability_resolution_proposal.png)

Alternatively, you can also open Duo Chat, select the source code and use the `/refactor` slash command.

![](assets/images/2024-03-duo-code-suggestions-tips/duo_code_suggestions_with_vulnerability_resolution_add_duo_chat_refactor.png)

A full example including a recording is available in the [GitLab Duo Examples documentation](https://docs.gitlab.com/ee/user/gitlab_duo_examples.html#explain-and-resolve-vulnerabilities). 

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/Ypwx4lFnHP0" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->



## More Tips 

### Verify code quality and security

More generated code requires quality assurance, testing, and security measures. Benefit from all features on a DevSecOps platform:

1. [CI/CD Components](https://docs.gitlab.com/ee/ci/components/) and [Pipeline Efficiency](https://docs.gitlab.com/ee/ci/pipelines/pipeline_efficiency.html)
1. [Code quality](https://docs.gitlab.com/ee/ci/testing/code_quality.html)
1. [Code test coverage](https://docs.gitlab.com/ee/ci/testing/code_coverage.html)
1. [Application security](https://docs.gitlab.com/ee/user/application_security/)
1. [Observability](https://docs.gitlab.com/ee/operations/)

### Learn as a team, and understand AI impact

Adapt and explore with dedicated team collaboration sessions, and record them for other teams to benefit later. You can also follow the [GitLab Duo Coffee Chat playlist on YouTube](https://www.youtube.com/playlist?list=PL05JrBw4t0Kp5uj_JgQiSvHw1jQu0mSVZ).

Read Taylor's blog post about [How to put generative AI to work in your DevSecOps environment](https://about.gitlab.com/blog/2024/03/07/how-to-put-generative-ai-to-work-in-your-devsecops-environment/), and how to measure the impact of AI using DORA metrics and [Value Streams dashboards](https://docs.gitlab.com/ee/user/analytics/value_streams_dashboard.html). Visit the [AI Transparency Center](https://about.gitlab.com/ai-transparency-center/) to learn more about data usage, transparency and AI ethics at GitLab.

### Development is a marathon, not a sprint

Somtimes, code suggestions might take a while to load, or do not come in an instant, compared to local auto-completion features. Take this time for your advantage, and think about the current algorithm or problem you are trying to solve. Often, a secondary thought can lead to more refined ideas. Or you take a short break to take a sip from your preferred drink, and continue refreshed when the suggestions arrive.

Some algorithms are super complex, or require code dependencies which cannot be resolved through auto-completion help. Proprietary and confidential code may provide less context to the Large Language Models, and therefore require more context in the comments for Code Suggestions. Follow your own pace and strategy, and leverage Code Suggestions in situations where they help with boilerplate code, or helper functions. 

### Contribute using GitLab Duo

You can use GitLab Duo to contribute to Open Source projects, using Code Suggestions, code refactoring, documentation through explainations, or test generation.

GitLab customers can [co-create GitLab using GitLab Duo](https://docs.gitlab.com/ee/user/gitlab_duo_examples.html#use-gitlab-duo-to-contribute-to-gitlab), too. Follow the updated guidelines for [AI-generated contributions](https://about.gitlab.com/community/contribute/dco-cla/#ai-generated-contributions), and watch an example recording from the GitLab Duo Coffee Chat: Contribute to GitLab using Code Suggestions and Chat:

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/TauP7soXj-E" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

## Conclusion

GitLab Duo Code Suggestions enables more efficient development workflows. It requires hands-on practice and exercise through tutorials, team workshops, and guided trainings. Automated workflows with Code Quality, Security Scanning and Observability ensure to tackle challenges with newly introduced source code in a much higher frequency. Taking advantage of all GitLab Duo features, including Chat, greatly improves the developer experience on the most comprehensive AI-powered DevSecOps platform.

Use the best practices in this blog tutorial to kickstart your journey, follow the [GitLab Duo documentation](https://docs.gitlab.com/ee/user/ai_features.html), and ask our teams for GitLab Duo AI workshops (I have already shadowed a customer workshop, they are great!). Please share your Code Suggestions feedback in [this issue](https://gitlab.com/gitlab-org/gitlab/-/issues/435783), including screenshots and videos (when possible). Thanks! 

