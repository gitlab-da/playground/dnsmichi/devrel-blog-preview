---
Title: Refactor legacy code into modern languages with AI-powered GitLab Duo 
---


## Multiple code suggestions 


## Experimental support for new languages 

PowerShell - https://gitlab.com/gitlab-org/gitlab-vscode-extension/-/issues/1396#note_1937495190 
Cicd config - YAML https://gitlab.com/gitlab-org/gitlab-vscode-extension/-/issues/1396#note_1982917818 



## Use Code Suggestions to add missing dependencies


The following snippet does not provide the necessary header includes.

```c
void host_lookup(char *user_supplied_addr) {
    struct hostent *hp;
    in_addr_t *addr;
    char hostname[64];
    in_addr_t inet_addr(const char *cp);

    /*routine that ensures user_supplied_addr is in the right format for conversion */

    validate_addr_form(user_supplied_addr);
    addr = inet_addr(user_supplied_addr);
    hp = gethostbyaddr( addr, sizeof(struct in_addr), AF_INET);
    strcpy(hostname, hp->h_name);
}
```

Use Duo Code Suggestions to add the missing header includes with the following steps:

1. Move the cursor into the first line
1. Add a comment `// Add missing includes`
1. Press Enter to trigger Code suggestions

For example

```c
// Add missing includes 

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/socket.h>
```

## Use Duo Chat /refactor to add missing dependencies

```markdown 
/refactor add missing dependencies
```


## Efficient answers

`Convert XY. Just give me the final answer` 

from https://gitlab.com/gitlab-org/gitlab/-/issues/430124#note_1993900056 



## Boilerplate code 

### Cross-platform version fetcher

```c
// Read OS files to identify the platform, name, versions
// Print them on the terminal
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef _WIN32
    #include <windows.h>
#elif __APPLE__
    #include <sys/utsname.h>
#else
    #include <sys/utsname.h>
#endif

void get_os_info() {
    #ifdef _WIN32
        OSVERSIONINFOEX info;
        ZeroMemory(&info, sizeof(OSVERSIONINFOEX));
        info.dwOSVersionInfoSize = sizeof(OSVERSIONINFOEX);
        GetVersionEx((OSVERSIONINFO*)&info);

        printf("Platform: Windows\n");
        printf("Version: %d.%d\n", info.dwMajorVersion, info.dwMinorVersion);
        printf("Build: %d\n", info.dwBuildNumber);
    #elif __APPLE__
        struct utsname sys_info;
        uname(&sys_info);

        printf("Platform: macOS\n");
        printf("Name: %s\n", sys_info.sysname);
        printf("Version: %s\n", sys_info.release);
    #else
        struct utsname sys_info;
        uname(&sys_info);

        printf("Platform: %s\n", sys_info.sysname);
        printf("Name: %s\n", sys_info.nodename);
        printf("Version: %s\n", sys_info.release);
    #endif
}

int main() {
    get_os_info();
    return 0;
}
```



## Identify common pain points 

Find common pain points in your daily workflows. Is it about Markdown, the development environment, or the CI/CD pipeline? Instead of opening a new browser search tab, ask GitLab Duo Chat.

### How to create a Python virtual environment



### Create Makefile

You can ask Duo Chat to create a generic Makefile based on instructions.

```markdown
Generate a Makefile that compiles C++ files, and takes the make target as variables for file names

Refine the Makefile to set the C++ version flag based on the filename, can be 03,11,14,etc

Refine the version parser regex to using the filenames 03.cpp 11.cpp 

Add a special case for 23.cpp which needs the 20 version compiler flag
```


![](assets/images/2024-07-duo-efficiency-v2/jetbrains_clion_cpp_duo_chat_makefile_01.png)
![](assets/images/2024-07-duo-efficiency-v2/jetbrains_clion_cpp_duo_chat_makefile_02.png)
![](assets/images/2024-07-duo-efficiency-v2/jetbrains_clion_cpp_duo_chat_makefile_03.png)
![](assets/images/2024-07-duo-efficiency-v2/jetbrains_clion_cpp_duo_chat_makefile_04.png)
![](assets/images/2024-07-duo-efficiency-v2/jetbrains_clion_cpp_duo_chat_makefile_05.png)

> Pro tip: You can also select the source code, and ask Duo Chat to create a Makefile for you.

![](assets/images/2024-07-duo-efficiency-v2/jetbrains_clion_c_create_makefile_from_selected_code.png)


### Embed a YouTube video in Markdown

I often need to embed a YouTube video in Markdown. There is a small trick to do this: Wrap the YouTube preview thumbnail with a URL to the video. But what is the syntax? Ask Duo Chat.

```markdown
Show how to embed a video in Markdown, using https://youtu.be/pwlDmLQMMPo
``` 

