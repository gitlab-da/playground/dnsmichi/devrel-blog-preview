# DevRel Blog Preview

Provides Markdown rendered blog previews through MkDocs and GitLab Pages.

> **Note**: This is not production ready. It is a boring solution to render blog post with Markdown and images to check formatting during writing, and being able to share the URL with team members.

## Usage

1. Create a new file in the [docs/](docs/) directory, following the pattern `YYYY-MM-DD-blog-title.md`. 
2. Edit away, git add, commit, push
3. Open the GitLab Pages URL and preview the content

Use Markdown, including image tags.

### Copying to Contentful

1. Copy the full text into [Contentful](https://handbook.gitlab.com/handbook/marketing/digital-experience/contentful-cms/editing-content/)
2. Replace the Markdown image tags with the actual image (copy-paste)

## Development

On macOS with Homebrew:

```
pip3 install -r requirements.txt

mkdocs serve
```
